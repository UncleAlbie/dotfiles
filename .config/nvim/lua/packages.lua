--------------------------------------------------------------------------------
-- Neovim configuration
--------------------------------------------------------------------------------
-- File: packages.lua
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- nvim-treesitter
--------------------------------------------------------------------------------
require'nvim-treesitter.configs'.setup {
	ensure_installed = {
		"bash",
		"regex",
		"c",
		"cpp",
		"make",
		"cmake",
		"python",
		"go",
		"lua",
		"dockerfile",
		"html",
		"json",
		"yaml",
		"css",
		"scss",
		"javascript",
		"typescript",
		"php",
		"rust",
	},

	sync_install = true,

	highlight = {
		enable = true,
		additional_vim_regex_highlighting = false,
	},
	indent = {
		enable = true
	},
}

--------------------------------------------------------------------------------
-- nvim-lspconfig
--------------------------------------------------------------------------------
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

local servers = { 'pylsp', 'clangd', 'gopls', 'tsserver', 'angularls' }
for _, lsp in pairs(servers) do
	require('lspconfig')[lsp].setup {
		on_attach = on_attach,
	}
end

require'lspconfig'.rust_analyzer.setup({
	on_attach=on_attach,
	cmd = {'rustup', 'run', 'stable', 'rust-analyzer'},
	settings = {
		["rust-analyzer"] = {
			imports = {
				granularity = {
					group = "module",
				},
				prefix = "self",
			},
			cargo = {
				buildScripts = {
					enable = true,
				},
			},
			procMacro = {
				enable = true
			},
		}
	}
})

require'lspconfig'.lua_ls.setup {
	on_attach = on_attach,
	settings = {
		Lua = {
			runtime = {
				-- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
				version = 'LuaJIT',
			},
			diagnostics = {
				-- Get the language server to recognize the `vim` global
				globals = {'vim'},
			},
			workspace = {
				-- Make the server aware of Neovim runtime files
				library = vim.api.nvim_get_runtime_file("", true),
				checkThirdParty = false,
			},
			-- Do not send telemetry data containing a randomized but unique identifier
			telemetry = {
				enable = false,
			},
		},
		single_file_support = true,
	},
}

-- nvim-cmp setup
local cmp = require 'cmp'
cmp.setup {
	mapping = cmp.mapping.preset.insert({
		['<C-d>'] = cmp.mapping.scroll_docs(-4),
		['<C-f>'] = cmp.mapping.scroll_docs(4),
		['<C-Space>'] = cmp.mapping.complete(),
		['<CR>'] = cmp.mapping.confirm {
			behavior = cmp.ConfirmBehavior.Replace,
			select = true,
		},
		['<Tab>'] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.confirm({ select = true })
			else
				fallback()
			end
		end, {"i","s",}),
	}),
	sources = {
		{ name = 'nvim_lsp' },
		{ name = 'buffer' },
		{ name = 'path' },
		{ name = 'nvim_lsp_signature_help' },
	},
}

--------------------------------------------------------------------------------
-- Comment
--------------------------------------------------------------------------------
require('Comment').setup()

--------------------------------------------------------------------------------
-- Guess Indent
--------------------------------------------------------------------------------
require('guess-indent').setup()

--------------------------------------------------------------------------------
-- FZF
--------------------------------------------------------------------------------
require('fzf-lua').setup({
	winopts = {
		height = 1,
		width = 1,
		preview ={
			scrollbar = false
		}
	}
})

--------------------------------------------------------------------------------
-- gitsigns
--------------------------------------------------------------------------------
require('gitsigns').setup()
-- 	signs = {
-- 		add			 = {hl = 'GruvboxAquaBold'	 , text = '│', numhl='GitSignsAddNr'   , linehl='GitSignsAddLn'},
-- 		change		 = {hl = 'GruvboxYellowBold', text = '│', numhl='GitSignsChangeNr', linehl='GitSignsChangeLn'},
-- 		delete		 = {hl = 'GruvboxRedBold', text = '_', numhl='GitSignsDeleteNr', linehl='GitSignsDeleteLn'},
-- 		topdelete	 = {hl = 'GruvboxRedBold', text = '‾', numhl='GitSignsDeleteNr', linehl='GitSignsDeleteLn'},
-- 		changedelete = {hl = 'GruvboxYellowBold', text = '~', numhl='GitSignsChangeNr', linehl='GitSignsChangeLn'},
-- 	},
-- 	linehl = false
-- })
--
--------------------------------------------------------------------------------
-- indent-blankline
--------------------------------------------------------------------------------
require("indent_blankline").setup {
	space_char_blankline = " ",
	show_current_context = false,
}

--------------------------------------------------------------------------------
-- autoclose
--------------------------------------------------------------------------------
--require("autoclose").setup()

--------------------------------------------------------------------------------
-- lualine
--------------------------------------------------------------------------------
require('lualine').setup {
	options = {
		icons_enabled = false,
	},
	tabline = {
		lualine_a = {'buffers'},
		lualine_b = {},
		lualine_c = {},
		lualine_x = {},
		lualine_y = {},
		lualine_z = {'tabs'}
	},
}
