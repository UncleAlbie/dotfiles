--------------------------------------------------------------------------------
-- Neovim configuration
--------------------------------------------------------------------------------
-- File: statusline.lua
--------------------------------------------------------------------------------

local cmd = vim.api.nvim_command

--------------------------------------------------------------------------------
-- Colors
--------------------------------------------------------------------------------
cmd('hi link Status GruvboxBg4')
local status_hl = '%#Status#'

--------------------------------------------------------------------------------
-- Definitions
--------------------------------------------------------------------------------
local status_left_pad = ' '
local status_right_pad = ' '

local field_left_pad = ' '
local field_right_pad = ' '

--------------------------------------------------------------------------------
-- Functions
--------------------------------------------------------------------------------
local function highlighter(field, highlighter)
	return '%#' .. highlighter .. '#' .. field .. status_hl
end

local function filename_modifier(filename)
	if string.sub(filename, 1, 7) == 'term://' then
		return highlighter('TERMINAL', 'GruvboxAquaBold')
	elseif vim.bo.readonly then
		return highlighter(filename .. ' (read-only)', 'GruvboxOrange')
	elseif vim.bo.modified then
		return highlighter(filename, 'GruvboxRedBold')
	else
		return highlighter(filename, 'GruvboxBlueBold')
	end
end

local function create_field(field)
	if field ~= '' then
			return field_left_pad .. field .. field_right_pad
	else
		return ''
	end
end

local function git_cur_branch()
	if vim.b.git_cur_branch ~= nil then
		return highlighter(vim.b.git_cur_branch, 'GruvboxPurple')
	end
	return ''
end

local function diagnostics(bufn)
	local errors = #vim.diagnostic.get(bufn, { severity = vim.diagnostic.severity.ERROR })
	local warnings = #vim.diagnostic.get(bufn, { severity = vim.diagnostic.severity.WARN })
	local info = #vim.diagnostic.get(bufn, { severity = vim.diagnostic.severity.INFO })
	-- local hints = #vim.diagnostic.get(bufn, { severity = vim.diagnostic.severity.HINT })

	local result = {}

	if errors > 0 then
		table.insert(result, highlighter(errors, 'GruvboxRedBold'))
	end
	if warnings > 0 then
		table.insert(result, highlighter(warnings, 'GruvboxYellowBold'))
	end
	if info > 0 then
		table.insert(result, highlighter(info, 'GruvboxBlueBold'))
	end
	--[[
	if hints > 0 then
		table.insert(result, highlighter(hints, 'GruvboxPurpleBold'))
	end
	]]

	return table.concat(result, ' ')
end

local function mode()
	local normal = 'NORMAL '
	local nop = 'NOP'
	local visual = highlighter('VISUAL ', 'GruvboxPurpleBold')
	local select = highlighter('SELECT ', 'GruvboxYellowBold')
	local insert = highlighter('INSERT ', 'GruvboxBlueBold')
	local replace = highlighter('REPLACE', 'GruvboxRedBold')
	local command = highlighter('COMMAND', 'GruvboxAquaBold')
	local prompt = highlighter('PROMPT ', 'GruvboxAquaBold')
	local shell = highlighter('SHELL  ', 'GruvboxGreenBold')
	local terminal = highlighter('TERM   ', 'GruvboxGreenBold')
	local null = 'NULL'

	local modes = {
		n = normal,
		no = nop,
		nov = nop,
		noV = nop,
		['noCTRL-V'] = nop,
		niI = normal,
		niR = normal,
		niV = normal,
		v = visual,
		V = visual,
		['^V'] = visual,
		['CTRL-V'] = visual,
		s = select,
		S = select,
		['CTRL-S'] = select,
		i = insert,
		ic = insert,
		ix = insert,
		R = replace,
		Rc = replace,
		Rv = replace,
		Rx = replace,
		c = command,
		cv = command,
		ce = command,
		r = prompt,
		rm = prompt,
		['r?'] = prompt,
		['!'] = shell,
		t = terminal,
		['null'] = null,
	}
	return modes[vim.fn.mode()] or vim.fn.mode()
end

local function status_left()
	return table.concat {
		create_field(mode()),
		-- create_field('%n'),
		create_field(filename_modifier('%f')),
		create_field(git_cur_branch()),
	}
end

local function status_center()
	return table.concat {
	}
end

local function status_right()
	return table.concat {
		create_field(diagnostics()),
		create_field('%c'),
		create_field('%l/%L'),
		create_field(string.upper(vim.bo.filetype)),
	}
end

--------------------------------------------------------------------------------
-- Components
--------------------------------------------------------------------------------
function Statusline()
	return table.concat {
		status_hl,
		status_left_pad,
		status_left(),

		'%=',

		status_hl,
		status_center(),

		'%=',

		status_hl,
		status_right(),
		status_right_pad,
	}
end

vim.o.statusline = '%!luaeval("Statusline()")'
-- vim.o.tabline = '%!luaeval("Tabline()")' -- TODO
