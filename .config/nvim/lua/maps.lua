--------------------------------------------------------------------------------
-- Neovim configuration
--------------------------------------------------------------------------------
-- File: maps.lua
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Functions
--------------------------------------------------------------------------------
local function map(mode, shortcut, command)
  vim.api.nvim_set_keymap(mode, shortcut, command, { noremap = true, silent = true })
end

local function nmap(shortcut, command)
  map('n', shortcut, command)
end

local function imap(shortcut, command)
  map('i', shortcut, command)
end

local function tmap(shortcut, command)
  map('t', shortcut, command)
end

local function vmap(shortcut, command)
  map('v', shortcut, command)
end

local function map_nit(shortcut, command)
  nmap(shortcut, command)
  imap(shortcut, '<C-\\><C-n>' .. command)
  tmap(shortcut, '<C-\\><C-n>' .. command)
end

--------------------------------------------------------------------------------
-- Mappings
--------------------------------------------------------------------------------
vim.g.mapleader = ","

-- Toggle fold
nmap('<space>', 'za')

-- Terminal
nmap('<leader>t', '<cmd>split | resize 15 | term<CR>')
tmap('<C-w>', '<C-\\><C-n><C-w>')

-- Netrw
nmap('<leader>n', '<cmd>Le<CR>')

-- Autoclose
imap('{<CR>', '{<CR>}<C-\\><C-n>O')
imap('[<CR>', '[<CR>]<C-\\><C-n>O')
imap('(<CR>', '(<CR>)<C-\\><C-n>O')

-- LSP
nmap('gd', '<cmd>lua vim.lsp.buf.definition()<CR>')
nmap('gD', '<cmd>lua vim.lsp.buf.declaration()<CR>')
nmap('K', '<cmd>lua vim.lsp.buf.hover()<CR>')
nmap('<leader>rn', '<cmd>lua vim.lsp.buf.rename()<CR>')

-- Navigation using ALT
map_nit('<A-h>', '<C-w><C-h>')
map_nit('<A-j>', '<C-w><C-j>')
map_nit('<A-k>', '<C-w><C-k>')
map_nit('<A-l>', '<C-w><C-l>')
map_nit('<A-H>', '<C-w><')
map_nit('<A-J>', '<C-w>-')
map_nit('<A-K>', '<C-w>+')
map_nit('<A-L>', '<C-w>>')
map_nit('<A-v>', '<C-w>v')
map_nit('<A-s>', '<C-w>s')
map_nit('<A-c>', '<C-w>c')
map_nit('<A-n>', '<cmd>bn<CR>')
map_nit('<A-p>', '<cmd>bp<CR>')
map_nit('<SA-n>', '<cmd>tabnext<CR>')
map_nit('<SA-p>', '<cmd>tabprevious<CR>')
map_nit('<A-d>', '<cmd>bd<CR>')
map_nit('<A-q>', '<cmd>q<CR>')
map_nit('<A-o>', '<cmd>tabedit %<CR>')
map_nit('<A-i>', '<cmd>tabclose<CR>')

-- FZF
nmap('<leader>ff', '<cmd>lua require(\'fzf-lua\').files()<CR>')
nmap('<leader>fh', '<cmd>lua require(\'fzf-lua\').oldfiles()<CR>')
nmap('<leader>fb', '<cmd>lua require(\'fzf-lua\').buffers()<CR>')
nmap('<leader>fgg', '<cmd>lua require(\'fzf-lua\').grep()<CR>')
nmap('<leader>fgc', '<cmd>lua require(\'fzf-lua\').grep_cword()<CR>')
vmap('<leader>fgv', '<cmd>lua require(\'fzf-lua\').grep_visual()<CR>')
nmap('<leader>gf', '<cmd>lua require(\'fzf-lua\').git_files()<CR>')
nmap('<leader>gc', '<cmd>lua require(\'fzf-lua\').git_commits()<CR>')
nmap('<leader>gbc', '<cmd>lua require(\'fzf-lua\').git_bcommits()<CR>')
nmap('<leader>gbb', '<cmd>lua require(\'fzf-lua\').git_branches()<CR>')
nmap('<leader>ca', '<cmd>lua require(\'fzf-lua\').lsp_code_actions()<CR>')
nmap('<leader>fd', '<cmd>lua require(\'fzf-lua\').lsp_document_diagnostics()<CR>')
nmap('<leader>wd', '<cmd>lua require(\'fzf-lua\').lsp_workspace_diagnostics()<CR>')
