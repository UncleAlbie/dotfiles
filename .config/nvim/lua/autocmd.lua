--------------------------------------------------------------------------------
-- Neovim configuration
--------------------------------------------------------------------------------
-- File: autocmd.lua
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Functions
--------------------------------------------------------------------------------
local function git_current_branch()
	local git_cmd = 'git -C ' .. vim.fn.expand('%:h') .. ' '
	local handle = io.popen(git_cmd .. 'rev-parse --symbolic-full-name --abbrev-ref HEAD 2>/dev/null')

	if handle then
		vim.b.git_cur_branch = handle:read('*l')
		handle:close()
	end
end

local function clang_format_on_save()
	local fr = io.open('.clang-format', 'r')
	if fr ~= nil then
		io.close(fr)
		vim.lsp.buf.format()
	end
end

--------------------------------------------------------------------------------
-- Autocmd
--------------------------------------------------------------------------------
vim.api.nvim_create_autocmd({'BufNewFile', 'BufRead'}, {
	pattern = 'Dockerfile.*',
	command = 'set filetype=dockerfile',
})

vim.api.nvim_create_autocmd({'FileType'}, {
	pattern = 'python',
	command = 'setlocal formatprg=black\\ --quiet\\ -',
})
vim.api.nvim_create_autocmd({'FileType'}, {
	pattern = {'c', 'cpp'},
	command = 'setlocal formatprg=clang-format\\ -',
})

vim.api.nvim_create_autocmd({'BufEnter', 'FileChangedShellPost', 'FocusGained'}, {
	callback = git_current_branch
})

vim.api.nvim_create_autocmd({'TermOpen', 'TermEnter'}, {
	pattern = '*',
	command = 'setlocal nonumber norelativenumber nocursorline signcolumn=no'
})

vim.api.nvim_create_autocmd({'BufEnter'}, {
	pattern = 'term://*',
	command = 'startinsert'
})

vim.api.nvim_create_autocmd({'TermOpen', 'TermEnter'}, {
	pattern = '*',
	command = 'startinsert'
})

vim.api.nvim_create_autocmd({'BufWritePre'}, {
	pattern = {'*.c', '*.cpp', '*.h'},
	callback = clang_format_on_save
})
