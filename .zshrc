#   Copyright 2021 UncleAlbie <me@unclealbie.com>
#
#   Permission is hereby granted, free of charge, to any person obtaining a
#   copy of this software and associated documentation files (the "Software"),
#   to deal in the Software without restriction, including without limitation
#   the rights to use, copy, modify, merge, publish, distribute, sublicense,
#   and/or sell copies of the Software, and to permit persons to whom the
#   Software is furnished to do so, subject to the following conditions:
#
#   The above copyright notice and this permission notice shall be included in
#   all copies or substantial portions of the Software.
#
#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#   DEALINGS IN THE SOFTWARE.
#
#   ---------------------------------------------------------------------------
#   Title:                                                                zshrc
#   ---------------------------------------------------------------------------
# {{{ SETUP
CACHE_DIR="${HOME}/.cache/zsh"
CONFIG_DIR="${HOME}/.config/zsh"
DATA_DIR="${HOME}/.local/share/zsh"

if [ ! -d "${CACHE_DIR}" ]; then
    mkdir -p "${CACHE_DIR}"
fi

HISTORY_SIZE=10000
HISTORY_FILE="${CACHE_DIR}/histfile"

USER_PROJECT_DIR="${HOME}/Documents/Projects"
USER_PROJECT_DOTFILES_SRC="${USER_PROJECT_DIR}/dotfiles"

EXT_DIR_COLORS="${HOME}/.dir_colors"
EXT_FUNCTIONS_DIR="${DATA_DIR}/zee_functions"
EXT_COMPLETIONS_DIR="${DATA_DIR}/completions"
EXT_PURE_DIR="${DATA_DIR}/pure"
EXT_FZF_KEYBINDING="/usr/share/fzf/shell/key-bindings.zsh"
EXT_SYNTAX_HIGHLIGHTING="/usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"

RUSTUP_HOME="${HOME}/.local/share/rustup"
CARGO_HOME="${HOME}/.local/share/cargo"

BLACK=$(tput setaf 0)
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
YELLOW=$(tput setaf 3)
BLUE=$(tput setaf 4)
MAGENTA=$(tput setaf 5)
CYAN=$(tput setaf 6)
WHITE=$(tput setaf 7)
RESET=$(tput sgr0)
# }}}
#   ---------------------------------------------------------------------------
# {{{ CONFIG: opts
setopt autocd extendedglob nomatch HIST_IGNORE_DUPS nohashdirs nohashcmds appendhistory inc_append_history
unsetopt beep notify
# }}}
# {{{ CONFIG: bindkey
bindkey -v

bindkey "^[[A" up-line-or-beginning-search
bindkey "^[[B" down-line-or-beginning-search
bindkey "\e[1~" beginning-of-line
bindkey "\e[4~" end-of-line
bindkey "\e[5~" beginning-of-history
bindkey "\e[6~" end-of-history
bindkey "\e[5~" history-search-backward
bindkey "\e[6~" history-search-forward
bindkey "\e[3~" delete-char
bindkey "\e[2~" quoted-insert
bindkey "\e[5C" forward-word
bindkey "\e[5D" backward-word
bindkey "\e[1;5C" forward-word
bindkey "\e[1;5D" backward-word
bindkey "\e[8~" end-of-line
bindkey "\eOc" forward-word
bindkey "\eOd" backward-word
bindkey "\eOH" beginning-of-line
bindkey "\eOF" end-of-line
bindkey "\e[H" beginning-of-line
bindkey "\e[F" end-of-line
# }}}
# {{{ CONFIG: completion
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'
zstyle ':completion:*' menu select
zstyle ':completion:*:*:*:*:descriptions' format '%F{green}-- %d --%f'
zstyle ':completion:*:*:*:*:corrections' format '%F{yellow}!- %d (errors: %e) -!%f'
zstyle ':completion:*:messages' format ' %F{purple} -- %d --%f'
zstyle ':completion:*:warnings' format ' %F{red}-- no matches found --%f'
zstyle :compinstall filename ~/.zshrc
# }}}
# {{{ CONFIG: history
HISTFILE=${HISTORY_FILE}
HISTSIZE=${HISTORY_SIZE}
SAVEHIST=${HISTORY_SIZE}

autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
# }}}
# {{{ CONFIG: fpath
fpath=( ${EXT_FUNCTIONS_DIR}/** ${EXT_COMPLETIONS_DIR} ${EXT_PURE_DIR} $fpath )
# }}}
# {{{ LOAD: prompt
autoload -U promptinit && promptinit
prompt pure

# zstyle :prompt:pure:prompt:success color '#d3869b'
# zstyle :prompt:pure:prompt:error color '#fb4934'
# zstyle :prompt:pure:path color '#83a598'
# zstyle :prompt:pure:git:branch color '#7c6f64'
# zstyle :prompt:pure:virtualenv color '#7c6f74'
# zstyle :prompt:pure:continuation color '#7c6f74'
# zstyle :prompt:pure:git:arrow color '#8ec07c'
# }}}
# {{{ LOAD: man
autoload -Uz man
alias run-help=man
# }}}
# {{{ LOAD: tmux-pane-name-scheme
#autoload -Uz tmux-pane-name-scheme
#tmux-pane-name-scheme
# }}}
# {{{ LOAD: syntax highlighting
test -r ${EXT_SYNTAX_HIGHLIGHTING} && source ${EXT_SYNTAX_HIGHLIGHTING}
# }}}
# {{{ LOAD: fzf
test -r ${EXT_FZF_KEYBINDING} && source ${EXT_FZF_KEYBINDING}
# }}}
# {{{ LOAD: dir_colors
test -r ${EXT_DIR_COLORS} && eval $(dircolors ${EXT_DIR_COLORS})
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
# }}}
# {{{ ENVIRONMENT: PATH
export GOPATH="${HOME}/.local/lib/go"
export PATH="${HOME}/.local/bin:${PATH}:${HOME}/.npm-global/bin:${GOPATH}:${GOPATH}/bin"
test -r ${CARGO_HOME}/env && source ${CARGO_HOME}/env
# }}}
# {{{ ENVIRONMENT: PAGER
export LESS="-rMFK"
export PAGER="less"
export MANPAGER="less +Gg"
# }}}
# {{{ ENVIRONMENT: STEAM
export STEAM_FRAME_FORCE_CLOSE=1
# }}}
# {{{ ENVIRONMENT: QT
export QT_QPA_PLATFORMTHEME=gtk2
# }}}
# {{{ ENVIRONMENT: DEFAULT APPS
export EDITOR="nvim"
export BROWSER="firefox"
export TERMINAL="gnome-terminal"
# }}}
# {{{ ALIAS: basics
alias c=clear
alias xo=xdg-open
# }}}
# {{{ ALIAS: dotfiles
alias dotfiles='git --git-dir=${USER_PROJECT_DOTFILES_SRC} --work-tree=${HOME}'
# }}}
# {{{ ALIAS: encrypt/decrypt
alias encrypt='function () { gpg --output "${2}" --symmetric --cipher-algo AES256 "${1}" }'
alias decrypt='function () { gpg --output "${2}" --decrypt "${1}" }'
# }}}
# {{{ ALIAS: grep
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias grep='grep --color=auto'
alias xzegrep='xzegrep --color=auto'
alias xzfgrep='xzfgrep --color=auto'
alias xzgrep='xzgrep --color=auto'
alias zegrep='zegrep --color=auto'
alias zfgrep='zfgrep --color=auto'
alias zgrep='zgrep --color=auto'
# }}}
# {{{ ALIAS: bat
alias ccat='/usr/bin/cat'
alias cat='bat'
# }}}
# {{{ ALIAS: sleeptimer
alias sleeptimer='function () { sleep $(( ${1} * 60 )) && systemctl suspend }'
# }}}
# {{{ ALIAS: ls/tree
alias ls='ls -hF --color=always --group-directories-first --hyperlink=always --sort=extension'
alias l='ls'
alias l.='l -d .*'
alias l1='l -1'
alias la='l -A'
alias la1='la -1'
alias ll='l -l'
alias lll='ll -Z'
alias lla='ll -A'
alias llla='lla -Z'

alias tree='tree -CL 2 --dirsfirst'
# }}}
#   ---------------------------------------------------------------------------
# {{{ COMPINIT
autoload -Uz compinit
compinit -u
# }}}
#   ---------------------------------------------------------------------------
#   Modeline:                                        vim:set foldmethod=marker:
#   ---------------------------------------------------------------------------
